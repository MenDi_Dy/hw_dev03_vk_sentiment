# HW_DEV03_vk_sentiment

Aiohttp web-server

Реализация алгоритма машинного обучения по обнаружению сентиментов в форме веб-сервиса

Для успешной работы требуется скопирвоать содержимое разархивированной папки http://vectors.nlpl.eu/repository/11/181.zip
в src/model/181. Весит более 2 ГБ

Также для работы с PostgreSQL требуется создать базу данных vk_post, а в ней таблицу vk_sen по шаблону:

CREATE TABLE public.vk_sen
(
    group_link character varying(50),
    post_count integer,
    sentiment character varying
) 

И ввести свои данные в src/vk/vk_post

Запускать через src/app/server.py