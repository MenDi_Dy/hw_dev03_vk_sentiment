from aiohttp import web
import aiohttp_jinja2
import jinja2
import sys

from app.setup_service.logs import init_logger
from app.api.routes import setup_routes
from app.setup_service.configurator import get_config


def start(args):
    app = web.Application()
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))
    app['config'] = get_config(args)
    setup_routes(app)
    init_logger(app['config'])
    web.run_app(app,
                host='127.0.0.2',
                port=app['config']['service']['port'],
                access_log=None,
                )


if __name__ == '__main__':
    start(sys.argv[1:])
