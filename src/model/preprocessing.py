import gensim
import numpy as np
import pymorphy2


def preprocessing(mas):
    # Preprocessing function

    # 0
    morph = pymorphy2.MorphAnalyzer()
    ls_train = []
    for line in mas.astype(str).str.split():
        words = []
        for word in line:
            p = morph.parse(word)[0]  # делаем разбор
            words.append(p.normal_form)
        ls_train.append(words)

    # 0.1 Ограничиваем наши посты до 28 слов, так как сеть принимает на вход вектора опредленного размера
    for i in range(len(ls_train)):
        ls_train[i] = ls_train[i][0:28]

    # 1
    x_embedded = []
    model1 = gensim.models.KeyedVectors.load('../model/181/model.model')
    for word in ls_train:
        x_embedded.append(model1[word])

    # 2
    x_1 = np.array(x_embedded)
    x_11 = []
    for i in range(len(x_1)):
        size = 28 - x_1[i].shape[0]
        x_11.append(np.pad(x_1[i], ((0, size), (0, 0)), 'constant', constant_values=(0)))

    # 3
    x_11 = np.array(x_11)

    # 4
    x_11 = np.expand_dims(x_11, axis=3)
    return x_11
