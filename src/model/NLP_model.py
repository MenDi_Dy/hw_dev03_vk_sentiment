import numpy as np
import pandas as pd

from keras.models import load_model

from src.model.preprocessing import preprocessing
from src.vk.vk_pars import vk_posts


def nlp_model(group_link, post_count):
    post = vk_posts(group_link, post_count)
    post = pd.Series(post)
    prep = preprocessing(post)

    keras_model = load_model('../model/keras_model.h5')

    prediction = keras_model.predict(prep)

    post_type = []
    for j in range(len(prediction)):
        i = np.where(prediction[j] == max(prediction[j]))

        listofindices = list(zip(i[0]))
        for indice in listofindices:
            for index in indice:
                if index == 0:
                    post_type.append(0)
                elif index == 1:
                    post_type.append(1)
                elif index == 2:
                    post_type.append(-1)
    return post_type
