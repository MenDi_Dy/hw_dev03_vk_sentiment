import vk_api

vk_session = vk_api.VkApi('Your_phone_number', 'Your_password')
vk_session.auth()
vk = vk_session.get_api()


def vk_posts(group_link, post_count):
    texts = []
    post = vk.wall.get(domain=group_link, count=post_count)
    for text in post['items']:
        texts.append(text['text'])
    return texts


